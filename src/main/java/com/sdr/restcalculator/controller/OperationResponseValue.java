package com.sdr.restcalculator.controller;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OperationResponseValue {

    private BigDecimal value;

    private OperationResponseValue(BigDecimal value) {
        this.value = value;
    }

    public static OperationResponseValue of(BigDecimal value) {
        return new OperationResponseValue(value);
    }
}
