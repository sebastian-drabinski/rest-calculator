package com.sdr.restcalculator.controller;

import com.sdr.restcalculator.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class OperationController {

    @Autowired
    private OperationService operationService;

    @PostMapping("/add")
    public OperationResponseValue add(double val1, double val2) {
        return OperationResponseValue.of(operationService.add(val1, val2));
    }

    @GetMapping("/div")
    public OperationResponseValue div(double val1, double val2) {
        try {
            return OperationResponseValue.of(operationService.div(val1, val2));
        } catch (ArithmeticException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
