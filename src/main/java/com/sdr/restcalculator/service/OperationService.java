package com.sdr.restcalculator.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class OperationService {

    public BigDecimal add(double val1, double val2) {
        return BigDecimal.valueOf(val1).add(BigDecimal.valueOf(val2));
    }

    public BigDecimal div(double val1, double val2) throws ArithmeticException {
        return BigDecimal.valueOf(val1).divide(BigDecimal.valueOf(val2), RoundingMode.HALF_UP);
    }
}
