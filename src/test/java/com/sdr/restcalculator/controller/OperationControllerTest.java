package com.sdr.restcalculator.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OperationControllerTest {

    @Autowired
    MockMvc mvc;

    private static Stream<Arguments> add() {
        return Stream.of(
                Arguments.of("2.0e100", "3.5e99", 23.5e99), //extremelyLargeNumbers
                Arguments.of("2222.5", "-1111.5", 1111d) //positivePlusNegative
        );
    }

    private static Stream<Arguments> div() {
        return Stream.of(
                Arguments.of("1", "2", 0.5), //integers
                Arguments.of("-0.5", "-0.25", 2d) //negativeDecimals
        );
    }

    @ParameterizedTest
    @MethodSource
    void add(String val1, String val2, Double expected) throws Exception {
        mvc.perform(post("/add")
                        .param("val1", val1)
                        .param("val2", val2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("value", is(expected)));
    }

    @Test
    void div_byZero() throws Exception {
        mvc.perform(get("/div")
                        .param("val1", "1")
                        .param("val2", "0"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException));
    }

    @ParameterizedTest
    @MethodSource
    void div(String val1, String val2, Double expected) throws Exception {
        mvc.perform(get("/div")
                        .param("val1", val1)
                        .param("val2", val2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("value", is(expected)));
    }
}