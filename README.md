# REST Calculator

Usługa REST oferująca wykonywanie operacji dodawania i dzielenia

## Użyte technologie

* Java 11
* Spring Boot 2.6.4
* Maven
* Lombok
* REST
* Swagger UI

## Uruchomienie aplikacji

Wymagana jest Java11.

Aplikację można uruchomić jednym z poniższych poleceń.

* `./mvnw spring-boot:run` w systemie Linux,
* `./mvnw.cmd spring-boot:run` w systemie Windows.

## Budowanie projektu

Projekt można zbudować Mavenem używając dołączonego skryptu (maven-wrapper), wykonując jedno z poniższych poleceń:

* `./mvnw clean install` w systemie Linux,
* `./mvnw.cmd clean install` w systemie Windows.

Wymagane do tego jest ustawienie lokalizacji JDK11 w zmiennej środowiskowej `JAVA_HOME`.

## Lista usług (REST API)

Swagger UI -> http://localhost:8080/swagger-ui.html
